# Aufgabe:

# Programm zur Berechnung einer Gesamtnote aus schriftlichen und mündlichen Leistungen
# schriftliche Leistungen zählen doppelt so viel wie mündliche.


# Funktion welche eine liste von noten einliest und in int werte umwandelt
def noteneingabe(notentyp):
    noten = input(f"Gebe alle {notentyp} Noten ein: ").split(",")

    fehler = False # Variable mit Datentyp Boolean oder kurz bool, kann False oder True sein.

    for i in range( len(noten) ):
        # überspeichert die orginalwerte in der liste mit int werten.
        noten[i] = int(noten[i])
        if noten[i] > 6 or noten[i] < 1 :   # Prüfung ob note gültig:  if note >= 1 and note <= 6
            print(f"Du hast eine ungültige {notentyp} Note eingegeben: {noten[i]}")
            fehler = True

    if fehler:
        exit()
    
    return noten



#funktionsaufruf
schriftlich_noten = noteneingabe("schriftliche")
muendlich_noten = noteneingabe("muendliche")

# Berechnung
gesamtnote = ((sum(schriftlich_noten) * 2) + sum(muendlich_noten)) / ( len(schriftlich_noten) * 2 + len(muendlich_noten) )

# Ausgabe
print(f"Gesamtnote: {gesamtnote}")

# Zusatz:
#- Überprüfen Sie ob nur Noten zwischen 1 und 6 eingegeben wurden
#- Lesen Sie die Noten aus einer Datei ein.