#messwerte.py 

# Messwerte einlesen 
# Format "4.5, 7.0 , 12, 3.1, -1.34, 0 , 11.99"

messwerte = input("Geben Sie die Messwerte ein:")

messwerte = messwerte.split(",")

if len(messwerte) < 5:
    print("Hinweis: Es müssen mind. 5 Werte eingegeben werden!")
    exit()

for i in range( len(messwerte) ):
    # Überprüfen ob string auch eine valide zahl ist.
    # Funktioniert nicht für Kommazahlen, aber für ganze Zahlen.
    if not messwerte[i].lstrip("-").isdigit():
        print(f"Wert {messwerte[i]} is nicht gültig")
        exit()
    
    # Umwandeln in float  // Leerzeichen müssen nicht unbedingt mit strip() entfernt werden.
    messwerte[i] = float(messwerte[i])  
    
    
print(f" Messwerte: {messwerte}")

# Sortieren der Liste
messwerte = sorted(messwerte)
#Alternative messwerte.sort()

mini = messwerte[0]  	# alternative:  mini = min(messwerte)
maxi = messwerte[-1]	# alternative: maxi = max(messwerte)
avg = sum(messwerte)/len(messwerte)

print(f" Messwerte sortiert: {messwerte}")
print(f" Größter Wert: {maxi}")
print(f" Kleinster Wert: {mini}")
print(f" Durchschnitt: {avg}")


